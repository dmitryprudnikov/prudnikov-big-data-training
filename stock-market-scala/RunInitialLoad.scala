import scala.sys.{env, exit}
import scala.sys.process._
import scala.language.postfixOps
import com.typesafe.config.{Config, ConfigFactory, ConfigList}

import java.io.File
import collection.JavaConverters._


object RunInitialLoad {

  def main(args: Array[String]): Unit =
    val (source, lending, config) = getParsedArguments(args)
    val (dbConfig, scriptsPaths, processingMethods) = getConfig(config)
    val dbConnectionParametrs = getDBConnectionParameters(dbConfig)

    runDataBaseSetup(dbConnectionParametrs, scriptsPaths)
    compileAndRunMoveDataToLending(source, lending, scriptsPaths)
    createGrowthCalculatorFatJar()
    processAll(processingMethods, lending, config, scriptsPaths)


  def getParsedArguments(args: Array[String]): (String, String, String) =
    def nextArgument(parsedArgs: Map[String, String], remainingArgs: List[String]): (String, String, String) =
      remainingArgs match
        case Nil =>
          (parsedArgs("source"), parsedArgs("lending"), parsedArgs("config"))

        case ("--source" | "-src") :: value :: tail =>
          nextArgument(parsedArgs ++ Map("source" -> value), tail)

        case ("--lending" | "-lndg") :: value :: tail =>
          nextArgument(parsedArgs ++ Map("lending" -> value), tail)

        case ("--config" | "-c") :: value :: tail =>
          nextArgument(parsedArgs ++ Map("config" -> value), tail)

        case argument :: tail =>
          println(s"Unknown argument $argument")
          exit(1)

    val usage: String = "Usage: RunInitialLoad -src <source> -lndg <lending> -c <config>"
    if args.length != 6 then
      println(usage)
      exit(1)

    nextArgument(Map(), args.toList)


  def getConfig(pathToConfig: String): (Config, Config, List[String]) =
    val config = ConfigFactory
      .parseFile(new File(pathToConfig))
      .getConfig("com.company")

    (config.getConfig("mysql"), config.getConfig("scripts-paths"),
      config.getStringList("processing-methods").asScala.toList)


  def getDBConnectionParameters(dbConfig: Config): (String, String, String, String) =
    val host = env(dbConfig.getString("host"))
    val userName = env(dbConfig.getString("username"))
    val password = env(dbConfig.getString("password"))
    val database = env(dbConfig.getString("database"))

    (host, userName, password, database)


  def runDataBaseSetup(connectionParameters: (String, String, String, String), pathsToScripts: Config): Unit =
    val (host, user, password, database) = connectionParameters

    val createDataBaseScript = pathsToScripts.getString("create-data-base")

    val ddlTablesScriptsDir = new File(pathsToScripts.getString("ddl-tables-scripts"))
    val ddlTablesScripts = ddlTablesScriptsDir.listFiles.filter(_.isFile).toList.map(_.toString)

    Seq("cat", s"$createDataBaseScript") #|
      Seq("mysql", "-h", s"$host", "-u", s"$user", s"-p$password") !

    for script <- ddlTablesScripts do
      Seq("cat", s"$script") #|
        Seq("mysql", "-h", s"$host", "-u", s"$user", s"-p$password", "-D", s"$database") !


  def compileAndRunMoveDataToLending(src: String, dst: String, pathsToScripts: Config): Unit =
    val moveDataToLendingCompiledDir = pathsToScripts.getString("move-data-to-lending-compiled-dir")
    val moveDataToLendingScript = pathsToScripts.getString("move-data-to-lending")
    val compiledMoveDataToLendingScript = pathsToScripts.getString("move-data-to-lending-compiled")

    Seq("rm", "-r", s"$moveDataToLendingCompiledDir") !

    Seq("mkdir", s"$moveDataToLendingCompiledDir") !

    Seq("scalac", "-d", s"$moveDataToLendingCompiledDir", s"$moveDataToLendingScript") !

    Seq("scala", "-cp", s"$moveDataToLendingCompiledDir", s"$compiledMoveDataToLendingScript",
      "-src", s"$src", "-dst", s"$dst") !


  def createGrowthCalculatorFatJar(): Unit =
    Seq("sbt", "clean") !

    Seq("sbt", "assembly") !


  def runDataProcessing(pathToProcessingJar: String, dataProcessingArgs: (String, String, String)): Unit =
    val (pathToData, processingMethod, config) = dataProcessingArgs

    Seq("java", "-jar", s"$pathToProcessingJar", "-dp", s"$pathToData",
      "-pm", s"$processingMethod", "-c", s"$config") !


  def processAll(processingMethods: List[String], pathToLending: String,
                 pathToConfig: String, pathsToScripts: Config): Unit =
    val lendingDir: File = new File(pathToLending)
    val dataDirs = lendingDir.listFiles.toList.map(_.toString)
    val pathToProcessingJar = pathsToScripts.getString("data-processing-jar")

    for (dataDir <- dataDirs; processingMethod <- processingMethods) do
      runDataProcessing(pathToProcessingJar, (dataDir, processingMethod, pathToConfig))


}
