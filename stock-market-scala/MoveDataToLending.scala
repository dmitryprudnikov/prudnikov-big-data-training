import java.io.File
import scala.sys.exit
import scala.sys.process._
import scala.language.postfixOps


object MoveDataToLending {


  def main(args: Array[String]): Unit =
    val (pathToSource, pathToDestination) = getParsedArguments(args)
    val (sourceDir, destinationDir) = (new File(pathToSource), new File(pathToDestination))

    checkSourceDestinationExistence(sourceDir, destinationDir)
    if !checkDestinationIsEmpty(destinationDir) then
      reloadDestinationFolder(pathToDestination)

    val filesNames = getListOfFilesNames(sourceDir)
    val filesNamesWithTargetDir = getListOfFilesNamesWithTargetDir(filesNames)

    createTargetDirs(filesNamesWithTargetDir, pathToDestination)
    copyFromSrcToDst(filesNamesWithTargetDir, pathToSource, pathToDestination)


  def getParsedArguments(args: Array[String]): (String, String) =
    def nextArgument(parsedArgs: Map[String, String], remainingArgs: List[String]): (String, String) =
      remainingArgs match
        case Nil =>
          (parsedArgs("source"), parsedArgs("destination"))

        case ("--source" | "-src") :: value :: tail =>
          nextArgument(parsedArgs ++ Map("source" -> value), tail)

        case ("--destinaton" | "-dst") :: value :: tail =>
          nextArgument(parsedArgs ++ Map("destination" -> value), tail)

        case argument :: tail =>
          println(s"Unknown argument $argument")
          exit(1)

    val usage: String = "Usage: MoveDataToLending -src <src> -dst <dst>"
    if args.length != 4 then
      println(usage)
      exit(1)

    nextArgument(Map(), args.toList)


  def checkSourceDestinationExistence(srcDir: File, dstDir: File): Unit =
    if !srcDir.exists || !srcDir.isDirectory then
      println(s"Directory $srcDir does not exists")
      exit(1)

    if !dstDir.exists || !dstDir.isDirectory then
      println(s"Directory $dstDir does not exists")
      exit(1)


  def checkDestinationIsEmpty(dstDir: File): Boolean =
    dstDir.list.length == 0


  def reloadDestinationFolder(pathToDst: String): Unit =
    Seq("rm", "-r", pathToDst) !

    Seq("mkdir", pathToDst) !


  def getListOfFilesNames(srcDir: File): List[String] =
    for file <- srcDir.listFiles.filter(_.isFile).toList yield
      val filePath = file.toString
      filePath.substring(filePath.lastIndexOf("/") + 1)


  def getFileTargetDirTuple(fileName: String) =
    val Array(stockName, _, stockType, _, _) = fileName.split("_")
    (fileName, s"${stockName}_${stockType}")


  def getListOfFilesNamesWithTargetDir(filesNames: List[String]) =
    for fileName <- filesNames yield getFileTargetDirTuple(fileName)


  def createTargetDirs(filesWithTargetDir: List[(String, String)], pathToDst: String): Unit =
    val dirs = for fileWithTargetDir <- filesWithTargetDir yield fileWithTargetDir._2
    dirs.toSet.foreach(dir => Seq("mkdir", s"$pathToDst/$dir") !)


  def copyFromSrcToDst(filesWithTargetDirs: List[(String, String)], pathToSrc: String, pathToDst: String): Unit =
    for fileWithTargetDir <- filesWithTargetDirs do
      val (fileName, targetDir) = fileWithTargetDir
      Seq("cp", s"$pathToSrc/$fileName", s"$pathToDst/$targetDir/$fileName") !


}
