DROP TABLE IF EXISTS `stocksGrowthSummaryOCAvg`;
CREATE TABLE `stocksGrowthSummaryOCAvg` (
	`name` VARCHAR(20),
	`bid` BOOLEAN,
	`january` DECIMAL(7,4),
	`february` DECIMAL(7,4) ,
	`march` DECIMAL(7,4),
	`april` DECIMAL(7,4),
	`may` DECIMAL(7,4),
	`june` DECIMAL(7,4),
	`july` DECIMAL(7,4),
	`august` DECIMAL(7,4),
	`september` DECIMAL(7,4),
	`october` DECIMAL(7,4),
	`november` DECIMAL(7,4),
	`december` DECIMAL(7,4),
	`average` DECIMAL(7,4),
	PRIMARY KEY (`name`, `bid`)
);