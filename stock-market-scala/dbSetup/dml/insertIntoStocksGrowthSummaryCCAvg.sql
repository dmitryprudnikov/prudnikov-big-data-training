TRUNCATE stocksGrowthSummaryCCAvg;

INSERT INTO stocksGrowthSummaryCCAvg
SELECT name, 
	   bid,
	   AVG(january),
	   AVG(february),
	   AVG(march),
	   AVG(april),
	   AVG(may),
	   AVG(june),
	   AVG(july),
	   AVG(august),
	   AVG(september),
	   AVG(october),
	   AVG(november),
	   AVG(december),
	   AVG(average)
FROM stocksGrowthSummaryCC 
GROUP BY name, 
		 bid