#!/bin/bash

while [[ $# -gt 0 ]]; do

  case $1 in
    -src|--source)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        source=$2
        shift
        shift
      else
        echo "Error: argument for $1 is missing"
        exit 1
      fi
      ;;

    -lndg|--lending)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        lending=$2
        shift
        shift
      else
        echo "Error: argument for $1 is missing"
        exit 1
      fi
      ;;

    -c|--config)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        config=$2
        shift
        shift
      else
        echo "Error: argument for $1 is missing"
        exit 1
      fi
      ;;

    *)
      echo "Error: unsupported flag $1"
      exit 1

  esac

done

if [ -n "$source" ] && [ -n "$lending" ] && [ -n "$config" ]; then
  if [ -d "compiled" ]; then
    rm -r compiled
  fi

  mkdir compiled

  scalac -d compiled -cp lib/config-1.4.1.jar RunInitialLoad.scala
  scala -cp ./compiled -cp ./lib/config-1.4.1.jar RunInitialLoad -src "$source"  -lndg "$lending" -c "$config"
else
  echo "One of the required parameters is missing"
  exit 1
fi
