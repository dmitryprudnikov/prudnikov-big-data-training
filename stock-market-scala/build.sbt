name := "Get-stock-growth-summary-scala"

version := "0.1"

scalaVersion := "3.1.0"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.27",
  "com.typesafe" % "config" % "1.4.1"
)

mainClass in assembly := Some("com.company.Calculator")
