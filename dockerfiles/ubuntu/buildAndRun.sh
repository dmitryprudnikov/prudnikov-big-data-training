#!/bin/bash


for i in "$@"; do
  case $i in
    -u=*|--user=*)
      USER="${i#*=}"
      shift
      ;;
    -p=*|--password=*)
      PASSWORD="${i#*=}"
      shift 
      ;;
    *)
      # unknown option
      ;;
  esac
done


docker build -t "ubuntu-based" . --build-arg MYSQL_USER=${USER} --build-arg MYSQL_PASSWORD=${PASSWORD}


docker run -it "ubuntu-based"
