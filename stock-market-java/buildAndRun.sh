#!/bin/bash

function db_setup() {
  mysql --host="$1" --user="$2" --password="$3" < data-base-setup/createDataBase.sql
  mysql --host="$1" --user="$2" --password="$3" --database < data-base-setup/ddl/tables/createSummaryTable.sql
  mysql --host="$1" --user="$2" --password="$3" --database < data-base-setup/ddl/procedures/createProcedureGetAvereges.sql
}

while [[ $# -gt 0 ]]; do

  case $1 in
    -dbs|--dbsetup)
      db_setup "$MYSQL_HOST" "$MYSQL_USER" "$MYSQL_PASSWORD" "$MYSQL_STOCK_MARKET_DATABASE"
      shift
      ;;

    -dd|--data-dir)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        data_dir=$2
        shift
        shift
      else
        echo "Error: argument for $1 is missing"
        exit 1
      fi
      ;;

    -pm|--processing-method)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        processing_method=$2
        shift
        shift
      else
        echo "Error: argument for $1 is missing"
        exit 1
      fi
      ;;

    *)
      echo "Error: unsupported flag $1"
      exit 1

  esac

done

if [ -n "$data_dir" ] && [ -n "$processing_method" ]; then
  mvn package
  java -jar target/Get-stock-growth-summary-java-maven-1.0-SNAPSHOT.jar -dd "$data_dir" -pm "$processing_method"
else
  echo "One of the required parameters is missing"
  exit 1
fi



