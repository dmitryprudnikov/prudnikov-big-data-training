package com.company

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

import java.io.File
import scala.annotation.tailrec
import scala.collection.JavaConverters.asScalaSetConverter
import scala.sys.exit


object AnnualStockGrowthCalculator {

  type Options = Map[String, String]
  type ReadParameters = (String, Map[String, String])
  type WriteParameters = (String, Map[String, String], String)

  val spark: SparkSession = SparkSession.builder()
    .appName("AnnualStockGrowthCalculator")
    .master("local")
    .getOrCreate()

  import spark.implicits._


  def main(args: Array[String]): Unit = {
    val (config, method) = parsedArguments(args)

    val sparkConfig = getSparkConfig(config)

    val readParameters = getReadParameters(sparkConfig)
    val writeParameters = getWriteParameters(sparkConfig)

    annualStockGrowthCalculationETL(readParameters, writeParameters, method)

    spark.close()
  }


  def parsedArguments(arguments: Array[String]): (String, String) = {
    @tailrec
    def loop(rest: List[String], parsed: Map[String, String]): (String, String) = {
      rest match {
        case Nil => (parsed("config"), parsed("method"))

        case ("-c" | "--config") :: value :: tail =>
          loop(tail, parsed + ("config" -> value))

        case ("-m" | "--method") :: value :: tail =>
          if (value == "open-close" || value == "close-close")
            loop(tail, parsed + ("method" -> value))
          else {
            println(s"Unknown processing method: $value")
            exit(1)
          }

        case argument :: _ =>
          println(s"Unknown argument: $argument"); exit(1)

      }
    }

    val usage = "Usage: AnnualStockGrowthCalculator -c <config> -m <method>"

    if (arguments.length != 4) {
      println(usage)
      exit(1)
    }

    loop(arguments.toList, Map())
  }


  def getSparkConfig(path: String): Config = {
    ConfigFactory.parseFile(new File(path))
      .getConfig("AnnualStockGrowthCalculator").getConfig("spark")
  }


  def getReadParameters(config: Config): ReadParameters = {
    val readConfig = config.getConfig("read-parameters")
    val format = readConfig.getString("format")
    val optionsConfig = readConfig.getConfig("options")
    val options = optionsConfig.root.keySet.asScala
      .map(key => key -> optionsConfig.getString(key)).toMap

    (format, options)
  }


  def getWriteParameters(config: Config): WriteParameters = {
    val writeConfig = config.getConfig("write-parameters")
    val format = writeConfig.getString("format")
    val mode = writeConfig.getString("mode")
    val optionsConfig = writeConfig.getConfig("options")
    val options = optionsConfig.root.keySet.asScala
      .map(key => key -> optionsConfig.getString(key)).toMap

    (format, options, mode)
  }


//  def getSparkSession(config: Config): SparkSession = {
//    SparkSession.builder
//      .appName(config.getString("app-name"))
//      .master(config.getString("master"))
//      .getOrCreate
//  }


  def annualStockGrowthCalculationETL(readParameters: ReadParameters,
                                      writeParameters: WriteParameters, method: String): Unit = {
    val (readFormat, readOptions) = readParameters
    val (writeFormat, writeOptions, writeMode) = writeParameters

    val sourceDataDF = read(readFormat, readOptions)

    val tenSecondsStockPriceMetricsDS = getTenSecondsStockPriceMetrics(sourceDataDF)
    val monthStockPriceMetricsDS = getMonthStockPriceMetrics(tenSecondsStockPriceMetricsDS)
    val stockPriceAnnualGrowthMetricsDS =
      getStockPriceAnnualGrowthMetrics(monthStockPriceMetricsDS, method)

    write(writeFormat, writeOptions, writeMode, stockPriceAnnualGrowthMetricsDS)
  }


  def read(format: String, options: Options): DataFrame = {
    spark.read.format(format).options(options).load
  }


  def write[T](format: String, options: Options, mode: String, ds: Dataset[T]): Unit = {
    ds.write.format(format).options(options).mode(mode).save
  }


  def getTenSecondsStockPriceMetrics(df: DataFrame): Dataset[StockPriceMetrics] = {
    def rowToMetrics(row: Row): StockPriceMetrics = {
      StockPriceMetrics(
        row.getAs[String]("name"),
        row.getAs[Boolean]("is_bid"),
        row.getAs[String]("Time (UTC)"),
        Some(row.getAs[String]("Open")
          .replace(",", ".").toFloat),
        row.getAs[String]("Close")
          .replace(",", ".").toFloat
      )
    }

    val name =
      regexp_extract(input_file_name, "(?=[^\\/]*$).*?(?=_)", 0)
    val isBid =
      regexp_extract(input_file_name, "(?=[^\\/]*$)(Ask|Bid)", 0) === "Bid"

    df
      .withColumn("name", name)
      .withColumn("is_bid", isBid)
      .map(rowToMetrics)
  }


  def getMonthStockPriceMetrics(ds: Dataset[StockPriceMetrics]): Dataset[StockPriceMetrics] = {
    def rowToMetrics(row: ((String, Boolean, String), (Option[Float], Float))): StockPriceMetrics = {
      val ((name, isBid, period), (base, current)) = row
      StockPriceMetrics(name, isBid, period, base, current)
    }

    ds
      .groupByKey(row => (row.name, row.isBid, row.period.slice(0, 7)))
      .mapValues(metrics => (metrics.base, metrics.current))
      .reduceGroups((prev, curr) => (prev._1, curr._2))
      .map(rowToMetrics)
  }


  def getStockPriceAnnualGrowthMetrics(ds: Dataset[StockPriceMetrics],
                                       method: String): Dataset[StockPriceAnnualGrowthMetrics] = {

    def preparedToOpenClose(ds: Dataset[StockPriceMetrics]): Dataset[StockPriceMetrics] =
      ds

    def preparedToCloseClose(ds: Dataset[StockPriceMetrics]): Dataset[StockPriceMetrics] = {
      val nameBidWindow = Window
        .partitionBy("name", "isBid")
        .orderBy("period")

      val base = lag("current", 1)
        .over(nameBidWindow)
        .as[Option[Float]]

      ds.withColumn("base", base).as[StockPriceMetrics]
    }

    def getGrowth(base: Option[Float], current: Float): Option[Float] = {
      base match {
        case Some(base) => Some((current - base) / base * 100)
        case _ => None
      }
    }

    def toMonthGrowthMetrics(row: StockPriceMetrics) = {
      StockPriceMonthGrowthMetrics(
        row.name,
        row.isBid,
        row.period.slice(0, 4).toInt,
        row.period.slice(5, 7).toInt,
        getGrowth(row.base, row.current)
      )
    }

    def pivot(key: (String, Boolean, Int), values: Iterator[(Int, Option[Float])],
              method: String): StockPriceAnnualGrowthMetrics = {
      val (name, isBid, year) = key
      val monthGrowthMap = values.toMap

      StockPriceAnnualGrowthMetrics(
        name,
        isBid,
        year,
        method,
        monthGrowthMap.getOrElse(1, null),
        monthGrowthMap.getOrElse(2, null),
        monthGrowthMap.getOrElse(3, null),
        monthGrowthMap.getOrElse(4, null),
        monthGrowthMap.getOrElse(5, null),
        monthGrowthMap.getOrElse(6, null),
        monthGrowthMap.getOrElse(7, null),
        monthGrowthMap.getOrElse(8, null),
        monthGrowthMap.getOrElse(9, null),
        monthGrowthMap.getOrElse(10, null),
        monthGrowthMap.getOrElse(11, null),
        monthGrowthMap.getOrElse(12, null)
      )
    }

    val preparedMetrics = method match {
      case "open-close" => preparedToOpenClose(ds)
      case "close-close" => preparedToCloseClose(ds)
    }

    preparedMetrics
      .map(toMonthGrowthMetrics)
      .groupByKey(row => (row.name, row.isBid, row.year))
      .mapValues(row => (row.month, row.growth))
      .mapGroups(pivot(_, _, method))

  }


}


case class StockPriceMetrics(
                              name: String,
                              isBid: Boolean,
                              period: String,
                              base: Option[Float],
                              current: Float
                            )


case class StockPriceMonthGrowthMetrics(
                                         name: String,
                                         isBid: Boolean,
                                         year: Int,
                                         month: Int,
                                         growth: Option[Float]
                                       )


case class StockPriceAnnualGrowthMetrics(
                                          name: String,
                                          isBid: Boolean,
                                          year: Int,
                                          method: String,
                                          january: Option[Float],
                                          february: Option[Float],
                                          march: Option[Float],
                                          april: Option[Float],
                                          may: Option[Float],
                                          june: Option[Float],
                                          july: Option[Float],
                                          august: Option[Float],
                                          september: Option[Float],
                                          october: Option[Float],
                                          november: Option[Float],
                                          december: Option[Float]
                                        )