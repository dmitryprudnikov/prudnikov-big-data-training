package com.company

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{
  col, first, input_file_name, lag, last, lit,
  month, regexp_extract, regexp_replace, split, year
}

import java.io.File
import scala.annotation.tailrec
import scala.collection.JavaConverters.asScalaSetConverter
import scala.sys.exit

/**
 * Utility to get month growth of stocks.
 *
 * @author Dmitry Prudnikov
 * @version 0.1
 */
object MonthStockGrowthCalculator {

  /**
   * Program entry point.
   *
   * @param args provided command line arguments.
   */
  def main(args: Array[String]): Unit = {

    val (configPath, processingMethod) = getParsedArguments(args)

    val sparkConfig = getConfig(configPath)

    val spark = getSparkSession(sparkConfig)

    val sourceContent = getSourceContent(sparkConfig, spark)

    val tenSecondsPricesMetrics = getTenSecondsPricesMetrics(sourceContent)
    val monthPricesMetrics = getMonthPricesMetrics(tenSecondsPricesMetrics)
    val priceMonthGrowthMetrics =
      getPriceMonthGrowthMetrics(monthPricesMetrics, processingMethod)

    writePricesMonthGrowthMetrics(priceMonthGrowthMetrics, sparkConfig)

  }


  /**
   * Function to get parsed command line arguments.
   *
   * @param arguments provided command line arguments.
   * @return argument name, argument value map.
   */
  def getParsedArguments(arguments: Array[String]): (String, String) = {

    @tailrec
    def nextArgument(remainingArguments: List[String],
                     parsedArguments: Map[String, String]): (String, String) = {
      remainingArguments match {
        case Nil =>
          (parsedArguments("config"), parsedArguments("processingMethod"))

        case ("-c" | "--config") :: value :: tail =>
          nextArgument(tail, parsedArguments + ("config" -> value))

        case ("-pm" | "--processing-method") :: value :: tail =>
          if (value == "open-close" || value == "close-close") {
            nextArgument(tail, parsedArguments + ("processingMethod" -> value))
          } else {
            println("Unknown processing method.")
            exit(1)
          }

        case argument :: _ => println(s"Unknown argument: $argument")
          exit(1)

      }

    }

    val usage: String =
      "Usage: MonthStockGrowthCalculator -c <config> -pm <processingMethod>"

    if (arguments.length != 4) {
      println(usage)
      exit(1)
    }

    nextArgument(arguments.toList, Map())

  }


  /**
   * Function to get application config.
   *
   * @param path path to config file.
   * @return application config.
   */
  def getConfig(path: String): Config = {
    ConfigFactory.parseFile(new File(path)).getConfig("app").getConfig("spark")
  }


  /**
   * Function to get spark session.
   *
   * @param config spark config.
   * @return spark session.
   */
  def getSparkSession(config: Config): SparkSession = {
    SparkSession.builder
      .master(config.getString("master"))
      .appName(config.getString("app-name"))
      .getOrCreate
  }


  /**
   * Function to get content of source.
   *
   * @param config spark config to set read configuration.
   * @param spark  spark session.
   * @return dataframe with source content.
   */
  def getSourceContent(config: Config, spark: SparkSession): DataFrame = {
    val readConfig = config.getConfig("read-parameters")
    val format = readConfig.getString("format")
    val optionsConfig = readConfig.getConfig("options")
    val options = optionsConfig
      .root.keySet.asScala.map(key => key -> optionsConfig.getString(key)).toMap

    spark.read.format(format).options(options).load
  }


  /**
   * Function to get 10 seconds open close prices metrics.
   *
   * @param data dataframe with source data.
   * @return dataframe with 10 seconds open close prices metrics.
   */
  def getTenSecondsPricesMetrics(data: DataFrame): DataFrame = {
    val dataWithFileName = data.select(
      regexp_extract(input_file_name, "[^/]*$", 0).alias("file_name"),
      col("Time (UTC)").alias("time_utc"),
      col("Open").alias("open_price"),
      col("Close").alias("close_price")
    )

    dataWithFileName
      .withColumn("name",
        split(col("file_name"), "_").getItem(0))
      .withColumn("price_type",
        split(col("file_name"), "_").getItem(2))
      .drop("file_name")
  }


  /**
   * Function to get month open close prices metrics.
   *
   * @param data dataframe with 10 seconds open close prices metrics.
   * @return dataframe with month open close prices metrics.
   */
  def getMonthPricesMetrics(data: DataFrame): DataFrame = {
    val nameTypeYearMonthWindow = Window
      .partitionBy("name", "price_type", "year", "month")
      .orderBy("time_utc")
      .rangeBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataWithYearMonthColumns = data
      .withColumn("year", year(col("time_utc")))
      .withColumn("month", month(col("time_utc")))

    dataWithYearMonthColumns
      .withColumn("open_price",
        first("open_price").over(nameTypeYearMonthWindow))
      .withColumn("close_price",
        last("close_price").over(nameTypeYearMonthWindow))
      .drop("time_utc")
      .distinct

  }


  /**
   * Function to get calculated month growth of stock prices according to
   * provided processing method. Growth is calculated with following formula:
   * (x1 - x0) / x0 * 100, where x0 is base price and x1 is current price.
   *
   * @param data             dataframe with month open close prices metrics.
   * @param processingMethod method to process data (open-close or close-close).
   * @return dataframe with calculated month growth of stock prices.
   */
  def getPriceMonthGrowthMetrics(data: DataFrame, processingMethod: String): DataFrame = {

    def getPreparedOpenCloseMetrics(data: DataFrame) = {
      data
        .withColumnRenamed("open_price", "base_price")
        .withColumnRenamed("close_price", "current_price")
    }

    def getPreparedCloseCloseMetrics(data: DataFrame) = {
      val nameTypeWindow = Window
        .partitionBy("name", "price_type")
        .orderBy("year", "month")

      data
        .withColumn("base_price",
          lag("close_price", 1).over(nameTypeWindow))
        .withColumnRenamed("close_price", "current_price")
        .drop("open_price")
    }

    val numberToMonth = Map(
      "1" -> "january", "2" -> "february", "3" -> "march", "4" -> "april",
      "5" -> "may", "6" -> "june", "7" -> "july", "8" -> "august",
      "9" -> "september", "10" -> "october", "11" -> "november", "12" -> "december",
    )

    val dataWithFloatPrices = data
      .withColumn("open_price",
        regexp_replace(col("open_price"), ",", "."))
      .withColumn("close_price",
        regexp_replace(col("close_price"), ",", "."))

    val preparedMetrics = processingMethod match {
      case "open-close" => getPreparedOpenCloseMetrics(dataWithFloatPrices)
      case "close-close" => getPreparedCloseCloseMetrics(dataWithFloatPrices)
    }

    val monthGrowthMetrics = preparedMetrics
      .withColumn("processing_method", lit(processingMethod))
      .withColumn("growth",
        (col("current_price") - col("base_price"))
          / col("base_price") * 100)
      .drop("base_price", "current_price")

    val pivotedMonthGrowthMetrics = monthGrowthMetrics
      .groupBy("name", "price_type", "processing_method", "year")
      .pivot("month")
      .sum("growth")

    numberToMonth
      .foldLeft(pivotedMonthGrowthMetrics)({ case (acc, (number, month)) =>
        acc.withColumnRenamed(number, month)
      })

  }


  /**
   * Function to write and save calculated metrics.
   *
   * @param data   calculated metrics.
   * @param config spark config to set write configuration.
   */
  def writePricesMonthGrowthMetrics(data: DataFrame, config: Config): Unit = {
    val writeConfig = config.getConfig("write-parameters")
    val format = writeConfig.getString("format")
    val mode = writeConfig.getString("mode")
    val optionsConfig = writeConfig.getConfig("options")
    val options = optionsConfig
      .root.keySet.asScala.map(key => key -> optionsConfig.getString(key)).toMap

    data.write.format(format).options(options).mode(mode).save

  }


}
