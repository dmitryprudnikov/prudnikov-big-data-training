drop procedure if exists month_stock_growth_avg_insert;

delimiter $$

create procedure  month_stock_growth_avg_insert()
begin
	insert into month_stock_growth_avg
	select name,
		   price_type,
		   processing_method,
		   avg(january),
		   avg(february),
		   avg(march),
		   avg(april),
		   avg(may),
		   avg(june),
		   avg(july),
		   avg(august),
		   avg(september),
		   avg(october),
		   avg(november),
		   avg(december),
		   avg(average)
	from month_stock_growth
	group by name,
			 price_type,
			 processing_method;
end$$

delimiter ;