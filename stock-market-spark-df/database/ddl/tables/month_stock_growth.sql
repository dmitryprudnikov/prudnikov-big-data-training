drop table if exists month_stock_growth;
create table month_stock_growth (
	name varchar(20),
	price_type varchar(3),
	processing_method varchar(20),
	year smallint,
	january decimal(7,4),
	february decimal(7,4),
	march decimal(7,4),
	april decimal(7,4),
	may decimal(7,4),
	june decimal(7,4),
	july decimal(7,4),
	august decimal(7,4),
	september decimal(7,4),
	october decimal(7,4),
	november decimal(7,4),
	december decimal(7,4),
	average decimal(7,4) as ((
		coalesce(january, 0) + coalesce(february, 0) + coalesce(march, 0)
		+ coalesce(april, 0) + coalesce(may, 0) + coalesce(june, 0)
		+ coalesce(july, 0) + coalesce(august, 0) + coalesce(september, 0)
		+ coalesce(october, 0) + coalesce(november, 0) + coalesce(december, 0)) / 12)
		stored,
	primary key (name,year,price_type, processing_method)
);
