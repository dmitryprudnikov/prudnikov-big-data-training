Command line application to calculate month growth of stocks using scala
and sql. Program can be ran at once or step by step using options (provi-
ded below).

Make program executable:
chmod +x ./run.sh

To run program at once:
./run.sh -c "/path/to/config"

To run program step by step:
./run.sh -c "path/to/config" -cd
./run.sh -c "path/to/config" -ct -cp
etc.

Required arguments:
-c, --config                      absolute path to config file.

Run options:
-il, --initial-load               direct way run initial load;
-cd, --create-database            run script to create database;
-ct, --create-tables              run scripts to create all tables;
-cp, --create-procedures          run scripts to create all procedures;
-ts, --truncate-staging           run scripts to truncate staging tables;
-tt, --truncate-target            run scripts to truncate target tables;
-ig, --insert-growth              run scripts to calculate and insert
                                  growth into  growth target table;
-iga, --insert-growth-avg         run scripts to calculate and insert
                                  average growth into average growth
                                  target table;
-bm2l, --build-move-to-lending    build move data to lending project;
-rm2l, --run-move-to-lending      move data to lending;
-bm2s, --build-move-to-staging    build move data to staging project;
-rm2s, --run-move-to-staging      move data to staging.
