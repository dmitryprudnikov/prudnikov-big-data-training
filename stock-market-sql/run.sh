#!/bin/bash

setFlags() {
  argumentsAmount="$#"
  while [[ "$#" -gt 0 ]]; do
    case "$1" in
    -c | --config)
      configPath="$2"
      shift
      shift
      ;;
    -il | --initial-load)
      initialLoadFlag=1
      shift
      ;;
    -cd | --create-database)
      createDatabaseFlag=1
      shift
      ;;
    -ct | --create-tables)
      createTablesFlag=1
      shift
      ;;
    -cp | --create-procedures)
      createProceduresFlag=1
      shift
      ;;
    -ts | --truncate-staging)
      truncateStagingTablesFlag=1
      shift
      ;;
    -tt | --truncate-target)
      truncateTargetTablesFlag=1
      shift
      ;;
    -ig | --insert-growth)
      insertIntoGrowthTableFlag=1
      shift
      ;;
    -iga | --insert-growth-avg)
      insertIntoGrowthAvgTableFlag=1
      shift
      ;;
    -bm2l | --build-move-to-lending)
      buildMoveDataToLendingFlag=1
      shift
      ;;
    -rm2l | --run-move-to-lending)
      runMoveDataToLendingFlag=1
      shift
      ;;
    -bm2s | --build-move-to-staging)
      buildInsertDataIntoStagingFlag=1
      shift
      ;;
    -rm2s | --run-move-to-staging)
      runInsertDataIntoStagingFlag=1
      shift
      ;;
    *)
      echo "Unknown parameter passed: $1"
      exit 1
      shift
      ;;
    esac
  done

  if [ ! -n "$configPath" ]; then
    echo "Configuration was not provided"
    exit 1
  fi

  if [ "$argumentsAmount" -eq 2 ]; then
    echo "Initial load"
    initialLoadFlag=1
  fi

}

setConfig() {
  config=$(cat $configPath |
    jq '{mysql:.app.mysql, scriptsPaths:.app."scripts-paths",
         dataPaths:.app."data-paths"}')

  mysqlConfig=$(echo $config | jq .mysql)
  scriptsPathsConfig=$(echo $config | jq .scriptsPaths)
  dataPathsConfig=$(echo $config | jq .dataPaths)

  hostEnv=$(echo $mysqlConfig | jq -r .host)
  userEnv=$(echo $mysqlConfig | jq -r .user)
  passwordEnv=$(echo $mysqlConfig | jq -r .password)
  databaseEnv=$(echo $mysqlConfig | jq -r .database)

  host=${!hostEnv}
  user=${!userEnv}
  password=${!passwordEnv}
  database=${!databaseEnv}

  createDatabaseScript=$(echo $scriptsPathsConfig | jq -r .createDatabaseScript)

  createTablesScripts=$(echo $scriptsPathsConfig | jq -r .ddlTablesScripts)

  createProceduresScripts=$(echo $scriptsPathsConfig |
    jq -r .ddlProceduresScripts)

  truncateStagingScripts=$(echo $scriptsPathsConfig |
    jq -r .truncateStagingScripts)
  truncateTargetScripts=$(echo $scriptsPathsConfig |
    jq -r .truncateTargetScripts)

  insertIntoStocksGrowthSummaryScripts=$(echo $scriptsPathsConfig |
    jq -r .insertIntoStocksGrowthSummaryScripts)
  insertIntoStocksGrowthSummaryAvgScripts=$(echo $scriptsPathsConfig |
    jq -r .insertIntoStocksGrowthSummaryAvgScripts)

  moveDataToLendingRootDir=$(echo $scriptsPathsConfig |
    jq -r .moveDataToLendingRootDir)
  moveDataToLendingBuildDir=$(echo $scriptsPathsConfig |
    jq -r .moveDataToLendingBuildDir)
  moveDataToLendingScript=$(echo $scriptsPathsConfig |
    jq -r .moveDataToLendingScript)
  moveDataToLendingCompiledScript=$(echo $scriptsPathsConfig |
    jq -r .moveDataToLendingCompiledScript)

  insertDataIntoStagingRootDir=$(echo $scriptsPathsConfig |
    jq -r .insertDataIntoStagingRootDir)
  insertDataIntoStagingFatJar=$(echo $scriptsPathsConfig |
    jq -r .insertDataIntoStagingFatJar)

  sourcePath=$(echo $dataPathsConfig | jq -r .sourcePath)
  lendingPath=$(echo $dataPathsConfig | jq -r .lendingPath)
}

setAllFlags1() {
  createDatabaseFlag=1
  createTablesFlag=1
  createProceduresFlag=1
  buildMoveDataToLendingFlag=1
  runMoveDataToLendingFlag=1
  truncateStagingTablesFlag=1
  buildInsertDataIntoStagingFlag=1
  runInsertDataIntoStagingFlag=1
  truncateTargetTablesFlag=1
  insertIntoGrowthTableFlag=1
  insertIntoGrowthAvgTableFlag=1
}

createDatabase() {
  mysql -h $1 -u $2 -p$3 <$4
}

executeSqlScripts() {
  for script in $5/*; do
    mysql -h $1 -u $2 -p$3 -D $4 <$script
  done
}

buildMoveDataToLending() {
  if [ -d $1 ]; then
    rm -r $1
  fi

  mkdir $1

  scalac -d $1 $2
}

runMoveDataToLending() {
  scala -cp $1 $2 -src $3 -dst $4
}

buildInsertDataIntoStaging() {
  sbt clean
  sbt assembly
}

runInsertDataIntoStaging() {
  java -jar $insertDataIntoStagingFatJar -src $1 -c $2
}

insertAllInStaging() {
  for dataDir in $1/*; do
    runInsertDataIntoStaging $dataDir $2
  done
}

run() {
  if [ -n "$initialLoadFlag" ]; then
    setAllFlags1
  fi

  if [ -n "$createDatabaseFlag" ]; then
    createDatabase $host $user $password $createDatabaseScript
  fi

  if [ -n "$createTablesFlag" ]; then
    executeSqlScripts $host $user $password $database $createTablesScripts
  fi

  if [ -n "$createProceduresFlag" ]; then
    executeSqlScripts $host $user $password $database $createProceduresScripts
  fi

  if [ -n "$buildMoveDataToLendingFlag" ]; then
    (
      cd $moveDataToLendingRootDir || exit 1
      buildMoveDataToLending $moveDataToLendingBuildDir $moveDataToLendingScript
    )
  fi

  if [ -n "$runMoveDataToLendingFlag" ]; then
    (
      cd $moveDataToLendingRootDir || exit 1
      runMoveDataToLending $moveDataToLendingBuildDir $moveDataToLendingCompiledScript \
        $sourcePath $lendingPath
    )
  fi

  if [ -n "$truncateStagingTablesFlag" ]; then
    executeSqlScripts $host $user $password $database $truncateStagingScripts
  fi

  if [ -n "$buildInsertDataIntoStagingFlag" ]; then
    (
      cd $insertDataIntoStagingRootDir || exit 1
      buildInsertDataIntoStaging
    )
  fi

  if [ -n "$runInsertDataIntoStagingFlag" ]; then
    (
      cd $insertDataIntoStagingRootDir || exit 1
      insertAllInStaging $lendingPath $configPath
    )
  fi

  if [ -n "$truncateTargetTablesFlag" ]; then
    executeSqlScripts $host $user $password $database $truncateTargetScripts
  fi

  if [ -n "$insertIntoGrowthTableFlag" ]; then
    executeSqlScripts $host $user $password $database \
      $insertIntoStocksGrowthSummaryScripts
  fi

  if [ -n "$insertIntoGrowthAvgTableFlag" ]; then
    executeSqlScripts $host $user $password $database \
      $insertIntoStocksGrowthSummaryAvgScripts
  fi
}

main() {
  setFlags "$@"
  setConfig
  run
}

main "$@"
