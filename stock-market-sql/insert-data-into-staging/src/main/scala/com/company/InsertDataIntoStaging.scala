package com.company

import com.typesafe.config.{Config, ConfigFactory}

import java.io.File
import java.sql.{Connection, DriverManager, PreparedStatement}
import scala.annotation.tailrec
import scala.io.Source
import scala.sys.{env, exit}

object InsertDataIntoStaging {

  case class PricesMetrics(dateTime: String, openPrice: String, closePrices: String)

  case class StockAnnualPricesMetrics(name: String, bid: Boolean, monthFirstPricesMetrics: List[PricesMetrics],
                                      monthLastPricesMetrics: List[PricesMetrics])


  def main(args: Array[String]): Unit = {
    val (source, config) = getParsedArguments(args)

    val (databaseConfig, templateStatementsConfig) = getConfig(config)
    val databaseConnectionParameters = getDatabaseConnectionParameters(databaseConfig)
    val insertTemplate = getInsertTemplate(templateStatementsConfig)

    val filesPaths = getListOfFilesPaths(source)
    val stockAnnualPricesMetricsList = getStockAnnualPricesMetricsList(filesPaths)

    insertProcessedDataIntoStaging(databaseConnectionParameters, insertTemplate, stockAnnualPricesMetricsList)

  }


  def getParsedArguments(arguments: Array[String]): (String, String) = {
    @tailrec
    def nextArgument(remainingArguments: List[String], parsedArguments: Map[String, String]): (String, String) = {
      remainingArguments match {
        case Nil => (parsedArguments("source"), parsedArguments("config"))

        case ("-src" | "--source") :: value :: tail =>
          nextArgument(tail, parsedArguments + ("source" -> value))

        case ("-c" | "--config") :: value :: tail =>
          nextArgument(tail, parsedArguments + ("config" -> value))

        case argument :: _ => println(s"Unknown argument: $argument")
          exit(1)

      }

    }

    val usage: String = "Usage: InsertDataIntoStaging -src <source> -c <config>"

    if (arguments.length != 4) {
      println(usage)
      exit(1)
    }

    nextArgument(arguments.toList, Map())

  }


  def getConfig(pathToConfig: String): (Config, Config) = {
    val config = ConfigFactory.parseFile(new File(pathToConfig)).getConfig("app")

    val databaseConfig = config.getConfig("mysql")
    val templateStatementsConfig = config.getConfig("jdbc-template-statements")

    (databaseConfig, templateStatementsConfig)
  }


  def getDatabaseConnectionParameters(dataBaseConfig: Config): (String, String, String) = {
    val user: String = env(dataBaseConfig.getString("user"))
    val password: String = env(dataBaseConfig.getString("password"))
    val url: String = env(dataBaseConfig.getString("url"))

    (user, password, url)
  }


  def getInsertTemplate(templateStatementsConfig: Config): String =
    templateStatementsConfig.getString("insertIntoMonthFirstLastStockPriceStaging")


  def getListOfFilesPaths(source: String): List[String] = {
    val sourceDir: File = new File(source)
    if (sourceDir.exists && sourceDir.isDirectory) {
      sourceDir.listFiles.filter(_.isFile).toList.map(_.toString)
    } else {
      println("Provided directory wasn't found.")
      exit(1)
    }

  }


  def getParsedFileName(pathToFile: String): (String, String) = {
    val fileName = pathToFile.substring(pathToFile.lastIndexOf("/") + 1)
    val Array(stockName, _, priceType, _, _) = fileName.split("_")

    (stockName, priceType)
  }


  def getFileContent(pathToFile: String): List[Array[String]] = {
    val source = Source.fromFile(pathToFile)
    try source.getLines.drop(1).map(_.split(";")).toList
    finally source.close()
  }


  def getMonthFirstLastPricesMetrics(fileContent: List[Array[String]]): List[(PricesMetrics, PricesMetrics)] = {

    @tailrec
    def nextIteration(remainingContent: List[Array[String]], previousMonth: String,
                      monthFirstPricesMetrics: PricesMetrics, monthLastPricesMetrics: PricesMetrics,
                      targetMetrics: List[(PricesMetrics, PricesMetrics)]): List[(PricesMetrics, PricesMetrics)] = {
      remainingContent match {

        case Nil => targetMetrics :+ (monthFirstPricesMetrics, monthLastPricesMetrics)

        case Array(dateTime, openPrice, _, _, closePrice, _) :: tail if dateTime.slice(5, 7) != previousMonth =>
          val updatedTargetMetrics = targetMetrics :+ (monthFirstPricesMetrics, monthLastPricesMetrics)
          val updatedMonthFirstPricesMetrics, updatedMonthLastPricesMetrics =
            PricesMetrics(dateTime, openPrice, closePrice)

          nextIteration(tail, dateTime.slice(5, 7), updatedMonthFirstPricesMetrics, updatedMonthLastPricesMetrics,
            updatedTargetMetrics)

        case Array(dateTime, openPrice, _, _, closePrice, _) :: tail =>
          val updatedMonthLastPricesMetrics = PricesMetrics(dateTime, openPrice, closePrice)

          nextIteration(tail, dateTime.slice(5, 7), monthFirstPricesMetrics, updatedMonthLastPricesMetrics, targetMetrics)

      }

    }

    val Array(dateTime, openPrice, _, _, closePrice, _) = fileContent.head
    val monthFirstPricesMetrics, monthLastPricesMetrics = PricesMetrics(dateTime, openPrice, closePrice)

    nextIteration(fileContent.tail, dateTime.slice(5, 7), monthFirstPricesMetrics, monthLastPricesMetrics, List())


  }


  def getStockAnnualPricesMetricsList(filesPaths: List[String]): List[StockAnnualPricesMetrics] = {
    for (filePath <- filesPaths) yield {
      val (stockName, stockType) = getParsedFileName(filePath)
      val fileContentList = getFileContent(filePath)
      val monthFirstLastPricesMetrics = getMonthFirstLastPricesMetrics(fileContentList)
      val (monthFirstPricesMetrics, monthLastPricesMetrics) = monthFirstLastPricesMetrics.unzip

      StockAnnualPricesMetrics(stockName, stockType.toLowerCase == "bid", monthFirstPricesMetrics, monthLastPricesMetrics)

    }

  }


  def getBatchInsertStatement(connection: Connection, insertTemplate: String,
                              processedData: List[StockAnnualPricesMetrics]): PreparedStatement = {

    def getPriceInInsertFormat(price: String): String = price.replace(",", ".")

    val batchInsertStatement = connection.prepareStatement(insertTemplate)

    for (stockAnnualPricesMetrics <- processedData) {

      batchInsertStatement.setString(1, stockAnnualPricesMetrics.name)
      batchInsertStatement.setBoolean(2, stockAnnualPricesMetrics.bid)

      for (pricesMetrics <- stockAnnualPricesMetrics.monthFirstPricesMetrics) {
        batchInsertStatement.setString(3, pricesMetrics.dateTime)
        batchInsertStatement.setString(4, getPriceInInsertFormat(pricesMetrics.openPrice))
        batchInsertStatement.setString(5, getPriceInInsertFormat(pricesMetrics.closePrices))
        batchInsertStatement.addBatch()
      }

      for (pricesMetrics <- stockAnnualPricesMetrics.monthLastPricesMetrics) {
        batchInsertStatement.setString(3, pricesMetrics.dateTime)
        batchInsertStatement.setString(4, getPriceInInsertFormat(pricesMetrics.openPrice))
        batchInsertStatement.setString(5, getPriceInInsertFormat(pricesMetrics.closePrices))
        batchInsertStatement.addBatch()
      }

    }

    batchInsertStatement

  }


  def insertProcessedDataIntoStaging(databaseConnectionParameters: (String, String, String), insertTemplate: String,
                                     processedData: List[StockAnnualPricesMetrics]): Unit = {

    val (user, password, url) = databaseConnectionParameters

    var connection: Connection = null
    try {
      connection = DriverManager.getConnection(s"jdbc:$url", user, password)

      val batchInsertStatement = getBatchInsertStatement(connection, insertTemplate, processedData)

      batchInsertStatement.executeBatch()

    } catch {
      case e: Exception => e.printStackTrace()
        exit(1)

    } finally {
      connection.close()
    }

  }


}
