name := "insert-data-into-staging"

version := "0.1"

scalaVersion := "2.12.10"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.27",
  "com.typesafe" % "config" % "1.4.1"
)

mainClass in assembly := Some("com.company.InsertDataIntoStaging")
