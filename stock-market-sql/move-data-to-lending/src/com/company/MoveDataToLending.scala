package com.company

import java.io.File
import scala.annotation.tailrec
import scala.sys.exit
import scala.sys.process._

object MoveDataToLending {


  def main(args: Array[String]): Unit = {
    val (pathToSource, pathToDestination) = getParsedArguments(args)
    val (sourceDir, destinationDir) = (new File(pathToSource), new File(pathToDestination))

    checkSourceDestinationExistence(sourceDir, destinationDir)
    if (!checkDestinationIsEmpty(destinationDir)){
      reloadDestinationFolder(pathToDestination)
    }

    val filesNames = getListOfFilesNames(sourceDir)
    val filesNamesWithTargetDirs = getListOfFilesNamesWithTargetDirs(filesNames)

    createTargetDirs(filesNamesWithTargetDirs, pathToDestination)
    copyFromSrcToDst(filesNamesWithTargetDirs, pathToSource, pathToDestination)

  }


  def getParsedArguments(arguments: Array[String]): (String, String) = {
    @tailrec
    def nextArgument(remainingArguments: List[String], parsedArguments: Map[String, String]): (String, String) = {
      remainingArguments match {
        case Nil => (parsedArguments("source"), parsedArguments("destination"))

        case ("-src" | "--source") :: value :: tail =>
          nextArgument(tail, parsedArguments + ("source" -> value))

        case ("-dst" | "--destination") :: value :: tail =>
          nextArgument(tail, parsedArguments + ("destination" -> value))

        case argument :: _ => println(s"Unknown argument: $argument")
          exit(1)

      }

    }

    val usage: String = "Usage: MoveDataToLending -src <src> -dst <dst>"

    if (arguments.length != 4) {
      println(usage)
      exit(1)
    }

    nextArgument(arguments.toList, Map())

  }


  def checkSourceDestinationExistence(sourceDir: File, destinationDir: File): Unit = {
    if (!sourceDir.exists || !sourceDir.isDirectory) {
      println(s"Directory $sourceDir does not exists")
      exit(1)
    }

    if (!destinationDir.exists || !destinationDir.isDirectory) {
      println(s"Directory $destinationDir does not exists")
      exit(1)
    }

  }


  def checkDestinationIsEmpty(destinationDir: File): Boolean =
    destinationDir.list.length == 0


  def reloadDestinationFolder(pathToDestination: String): Unit = {
    Seq("rm", "-r", pathToDestination).!
    Seq("mkdir", pathToDestination).!
  }


  def getListOfFilesNames(sourceDir: File): List[String] =
    for (filePath <- sourceDir.listFiles(_.isFile).toList.map(_.toString)) yield {
      filePath.substring(filePath.lastIndexOf("/") + 1)
    }


  def getListOfFilesNamesWithTargetDirs(filesNames: List[String]): List[(String, String)] =
    for (fileName <- filesNames) yield {
      val Array(stockName, _, stockType, _, _) = fileName.split("_")
      (fileName, s"${stockName}_${stockType}")
    }


  def createTargetDirs(filesWithTargetDir: List[(String, String)], pathToDestination: String): Unit = {
    val dirs = filesWithTargetDir.map(_._2).toSet
    dirs.foreach(dir => Seq("mkdir", s"$pathToDestination/$dir").!)
  }


  def copyFromSrcToDst(filesWithTargetDirs: List[(String, String)], pathToSource: String,
                       pathToDestination: String): Unit = {
    for ((fileName, targetDir) <- filesWithTargetDirs) {
      Seq("cp", s"$pathToSource/$fileName", s"$pathToDestination/$targetDir/$fileName").!
    }

  }


}
