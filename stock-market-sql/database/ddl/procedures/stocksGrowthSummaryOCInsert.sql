DROP PROCEDURE IF EXISTS stocksGrowthSummaryOCInsert;

DELIMITER $$

CREATE PROCEDURE  stocksGrowthSummaryOCInsert()
BEGIN
	
	INSERT INTO stocksGrowthSummary (name, year, bid, processingMethod,
	january, february, march, april, may, june, july, august,
	september, october,november,december)
	
	WITH
	
		dataWithYearMonthColumns AS(
			SELECT name,
				   bid,
				   datetime,
				   YEAR(datetime) AS year,
				   MONTH(datetime) AS month,
				   openPrice,
				   closePrice
			FROM monthFirstLastStockPriceStaging
		),
	
		partitionedDataWithMonthFirstLastPrices AS(
			SELECT name,
				   bid,
				   year,
				   month,
				   openPrice AS monthFirstOpenPrice,
				   LEAD(closePrice)
				        OVER nameBidYearMonthWindow AS monthLastClosePrice
			FROM dataWithYearMonthColumns
			WINDOW nameBidYearMonthWindow AS(
				PARTITION BY name,
							 bid,
							 year,
							 month
			    ORDER BY datetime
			)
		),
	
		countedGrowthData AS(
			SELECT name,
				   bid,
				   year,
				   month,
				   (monthLastClosePrice - monthFirstOpenPrice)
				   	   / monthFirstOpenPrice * 100 AS growth
			FROM partitionedDataWithMonthFirstLastPrices
			WHERE monthLastClosePrice IS NOT NULL
		)
	
	SELECT name,
		   year,
		   bid,
		   "open-close" AS processingMethod,
		   SUM(CASE WHEN month = 1 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 2 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 3 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 4 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 5 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 6 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 7 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 8 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 9 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 10 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 11 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 12 THEN growth ELSE NULL END)
	FROM countedGrowthData
	GROUP BY name,
		     bid,
		     year;
		     
END$$
	
DELIMITER ;