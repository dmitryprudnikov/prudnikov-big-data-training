DROP PROCEDURE IF EXISTS stocksGrowthSummaryCCInsert;

DELIMITER $$

CREATE PROCEDURE  stocksGrowthSummaryCCInsert()
BEGIN
	
	INSERT INTO stocksGrowthSummary (name, year, bid, processingMethod,
	january, february, march, april, may, june, july, august,
	september, october,november,december)
	
	WITH
	
		dataWithYearMonthColumns AS(
			SELECT name,
				   bid,
				   datetime,
				   YEAR(datetime) AS year,
				   MONTH(datetime) AS month,
				   closePrice
		    FROM monthFirstLastStockPriceStaging
	    ),
	
	 	partitionedDataWithMonthLastClosePrice AS(
			SELECT name,
				   bid,
				   datetime,
				   year,
				   month,
				   closePrice,
				   LAST_VALUE(closePrice)
				   	   OVER nameBidYearMonthWindow AS currentMonthLastClosePrice
			FROM dataWithYearMonthColumns
			WINDOW nameBidYearMonthWindow AS(
				PARTITION BY name,
							 bid,
							 year,
							 month
			 	ORDER BY datetime
			 	RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
			)
		),
	
		dataWithCurrentAndNextMonthClosePrices AS(
			SELECT name,
				   bid,
				   year,
				   month,
				   currentMonthLastClosePrice,
				   LAG(closePrice)
				   	   OVER nameBidWindow AS previousMonthLastClosePrice
			FROM partitionedDataWithMonthLastClosePrice
			WHERE closePrice = currentMonthLastClosePrice
			WINDOW nameBidWindow AS(
				PARTITION BY name,
							 bid
				ORDER BY datetime
			)
		),
	
		countedGrowthData AS(
			SELECT name,
				   bid,
				   year,
				   month,
				   (currentMonthLastClosePrice - previousMonthLastClosePrice)
				   	   / previousMonthLastClosePrice * 100 AS growth
			FROM dataWithCurrentAndNextMonthClosePrices
		)

	SELECT name,
		   year,
		   bid,
		   "close-close" AS processingMethod,
		   SUM(CASE WHEN month = 1 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 2 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 3 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 4 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 5 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 6 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 7 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 8 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 9 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 10 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 11 THEN growth ELSE NULL END),
		   SUM(CASE WHEN month = 12 THEN growth ELSE NULL END)
	FROM countedGrowthData
	GROUP BY name,
		     bid,
		     year;
		     
END$$
	
DELIMITER ;