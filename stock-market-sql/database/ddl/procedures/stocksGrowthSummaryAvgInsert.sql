DROP PROCEDURE IF EXISTS stocksGrowthSummaryAvgInsert;

DELIMITER $$

CREATE PROCEDURE  stocksGrowthSummaryAvgInsert()
BEGIN
	INSERT INTO stocksGrowthSummaryAvg
	SELECT name,
		   bid,
		   processingMethod,
		   AVG(january),
		   AVG(february),
		   AVG(march),
		   AVG(april),
		   AVG(may),
		   AVG(june),
		   AVG(july),
		   AVG(august),
		   AVG(september),
		   AVG(october),
		   AVG(november),
		   AVG(december),
		   AVG(average)
	FROM stocksGrowthSummary
	GROUP BY name,
			 bid,
			 processingMethod;
END$$

DELIMITER ;