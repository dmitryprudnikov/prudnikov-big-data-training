DROP TABLE IF EXISTS stocksGrowthSummary;
CREATE TABLE stocksGrowthSummary (
	name VARCHAR(20),
	year SMALLINT,
	bid TINYINT(1),
	processingMethod VARCHAR(20),
	january DECIMAL(7,4),
	february DECIMAL(7,4),
	march DECIMAL(7,4),
	april DECIMAL(7,4),
	may DECIMAL(7,4),
	june DECIMAL(7,4),
	july DECIMAL(7,4),
	august DECIMAL(7,4),
	september DECIMAL(7,4),
	october DECIMAL(7,4),
	november DECIMAL(7,4),
	december DECIMAL(7,4),
	average DECIMAL(7,4) AS((
		COALESCE(january, 0) + COALESCE(february, 0) + COALESCE(march, 0)
		+ COALESCE(april, 0) + COALESCE(may, 0) + COALESCE(june, 0)
		+ COALESCE(july, 0) + COALESCE(august, 0) + COALESCE(september, 0)
		+ COALESCE(october, 0) + COALESCE(november, 0) + COALESCE(december, 0)) / 12)
		STORED,
	PRIMARY KEY (name,year,bid, processingMethod)
);
