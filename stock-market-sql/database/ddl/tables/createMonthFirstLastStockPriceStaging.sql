DROP TABLE IF EXISTS monthFirstLastStockPriceStaging;
CREATE TABLE monthFirstLastStockPriceStaging (
	name VARCHAR(20),
	bid TINYINT(1),
	datetime DATETIME,
	openPrice FLOAT,
	closePrice FLOAT,
	PRIMARY KEY (name, bid, datetime)
);
