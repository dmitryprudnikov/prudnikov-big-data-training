package com.company

import sys.exit
import scala.io.Source
import java.io.File
import java.sql.{Connection, DriverManager, PreparedStatement}
import com.typesafe.config.{Config, ConfigFactory}


/**
 * Utility to calculate growth of stocks per month.
 *
 * @author Dmitry Prudnikov
 * @version 0.1
 */
object Calculator{


  /**
   * Program entry point.
   *
   * @param args provided command line arguments.
   */
  def main(args: Array[String]): Unit =
    val MYSQL_USER: String = sys.env("MYSQL_USER")
    val MYSQL_PASSWORD: String = sys.env("MYSQL_PASSWORD")
    val MYSQL_STOCK_MARKET_DATABASE: String = sys.env("MYSQL_STOCK_MARKET_DATABASE")
    val connectionParametrs: (String, String, String) =
      (MYSQL_USER, MYSQL_PASSWORD, MYSQL_STOCK_MARKET_DATABASE)

    val parsedArguments: Map[String, String] = getParsedArguments(args)
    val dataPath: String = parsedArguments("dataPath")
    val processingMethod: String = parsedArguments("processingMethod")
    val configPath: String = parsedArguments("configPath")

    val config: Config = getConfig(configPath)

    val insertQuery: String = getInsertQuery(config, processingMethod)

    val filesPaths: List[String] = getListOfFilesPaths(dataPath)

    val processedFiles: List[(String, String, String, Map[Int, Float])] =
      getProcessedFiles(filesPaths, processingMethod)

    println(processedFiles)

    // insertProcessedFilesIntoDB(processedFiles, connectionParametrs, insertQuery)


  /**
   * Function to get parsed command line argements.
   *
   * @param args provided command line arguments.
   * @return map where keys are the names of arguments
   *         and values are the values of these argument.
   */
  def getParsedArguments(args: Array[String]): Map[String, String]=
    def nextArgument(map: Map[String, String], list: List[String]): Map[String, String] =
      list match
        case Nil => map

        case ("--data-path" | "-dp") :: value :: tail =>
          nextArgument(map ++ Map("dataPath" -> value), tail)

        case ("--processing-method" | "-pm") :: value :: tail =>
          if (value == "open-close" | value == "close-close") then
            nextArgument(map ++ Map("processingMethod" -> value), tail)
          else
            println(s"Unknown processing method $value")
            exit(1)

        case ("--config-path" | "-cp") :: value :: tail =>
          nextArgument(map ++ Map("configPath" -> value), tail)

        case option :: tail => println(s"Unknown option $option")
          exit(1)

    val usage: String = "Usage: Calculator -dp <path/to/data> -pm <processing-method> -cp <config-path>"
    if args.length != 6 then
      println(usage)
      exit(1)

    val argsList: List[String] = args.toList
    nextArgument(Map(), argsList)


  /**
   * Function to get config from config file.
   *
   * @param pathToConfig path to config file.
   * @return parsed confid.
   */
  def getConfig(pathToConfig: String): Config =
    ConfigFactory.parseFile(new File(pathToConfig))


  /**
   * Function to get insert query according to
   * provided config and processing method.
   *
   * @param config parsed config with sql queries.
   * @param processingMethod method using which data will be processed.
   * @return string with sql query.
   */
  def getInsertQuery(config: Config, processingMethod: String): String =
    if processingMethod == "open-close" then
      config.getString("insertIntoStocksGrowthSummaryOC")
    else config.getString("insertIntoStocksGrowthSummaryCC")


  /**
   * Function to get list of files paths from provided directory.
   * If provided directory dosn't exist function ends the program
   * with status 1.
   *
   * @param dataPath path to directory where data is located.
   * @return list of files paths
   */
  def getListOfFilesPaths(dataPath: String): List[String] =
    val dir: File = new File(dataPath)
    if (dir.exists && dir.isDirectory) then
      dir.listFiles.filter(_.isFile).toList.map(_.toString).sorted
    else
      println("Provided directory wasn't found.")
      exit(1)


  /**
   * Function to get parsed file name: tuple of stock name, stock type and year.
   *
   * @param pathToFile path to file to be parsed.
   * @return tuple of stock name, stock type and year
   */
  def getParsedFileName(pathToFile: String): (String, String, String) =
    val fileName: String = pathToFile.substring(pathToFile.lastIndexOf("/") + 1)
    val Array(stockName, _, priceType, startDate, _) = fileName.split("_")
    (stockName, priceType, startDate.slice(0, 4))


  /**
   * Function to read file into a list.
   *
   * @param pathToFile path to file.
   * @return list of strings with file content.
   */
  def getFileContentList(pathToFile: String): List[String] =
    val source = Source.fromFile(pathToFile)
    try source.getLines.drop(1).toList finally source.close()


  /**
   * Function to calculate growth per every month using open-close method.
   *
   * @param fileContentList list with content of file.
   * @return map with calculated growth per every month.
   */
  def getOpenCloseGrowth(fileContentList: List[String]): Map[Int, Float] =

    /**
     * Function to make calculation: (y1 - x0) / x0 * 100, where
     * x0 is a first open price in month and y1 is last close price in month.
     *
     * @param monthFirstOpenPrice first open price in month.
     * @param monthLastClosePrice last close price in month.
     * @return calculated growth.
     */
    def getCalculatedGrowth(monthFirstOpenPrice: String, monthLastClosePrice: String): Float =
      val openPrice: Float = monthFirstOpenPrice.replace(",", ".").toFloat
      val closePrice: Float = monthLastClosePrice.replace(",", ".").toFloat
      (closePrice - openPrice) / openPrice * 100

    /**
     * Tail recursive function to iterate over the file and calculate growth.
     * Base case: growth calculation of the last month and return all calculated growths.
     * Recursive case: values update and growth calculation.
     *
     * @param fileContentList list with content of file.
     * @param previousMonth value of previos month.
     * @param monthFirstOpenPrice first open price in month.
     * @param monthLastClosePrice last close price in month.
     * @param growthPerMonthMap map where keys are months and values are growth of
     *                          corresponding month.
     * @return map with all calculated growth.
     */
    def iter(fileContentList: List[String], previousMonth: String, monthFirstOpenPrice: String,
             monthLastClosePrice: String, growthPerMonthMap: Map[Int, Float]): Map[Int, Float] =

      if fileContentList.isEmpty then
        val growth: Float = getCalculatedGrowth(monthFirstOpenPrice, monthLastClosePrice)
        growthPerMonthMap + (previousMonth.toInt -> growth)

      else
        val Array(dateTime, openPrice, _, _, closePrice, _) = fileContentList.head.split(";")
        val currentMonth: String = dateTime.slice(5, 7)

        if currentMonth != previousMonth then
          val growth: Float = getCalculatedGrowth(monthFirstOpenPrice, monthLastClosePrice)
          iter(fileContentList.tail, currentMonth, openPrice, closePrice,
            growthPerMonthMap + (previousMonth.toInt -> growth))

        else
          iter(fileContentList.tail, previousMonth, monthFirstOpenPrice, closePrice, growthPerMonthMap)

    val Array(dateTime, openPrice, _, _, closePrice, _) = fileContentList.head.split(";")

    iter(fileContentList.tail, dateTime.slice(5, 7), openPrice, closePrice, Map[Int, Float]())


  /**
   * Function to calculate growth per every month using close-close method.
   *
   * @param fileContentList list with content of file.
   * @param initialClosePrice previous close price for first month in file.
   *                          If there wasn't previous close price (first file
   *                          in directory) then first close price will be used instead.
   * @return map with calculated growth of every month and last close price of last
   *         month for further calculation.
   */
  def getCloseCloseGrowth(fileContentList: List[String], initialClosePrice: Float): (Map[Int, Float], Float) =

    /**
     * Function to make calculation: (x1 - x0) / x0 * 100, where
     * x0 is a last close price of previous month
     * and x1 is a last close price of curren month.
     *
     * @param prevMonthLastClosePrice last close price of previous month.
     * @param currMonthLastClosePrice last close price of curren month.
     * @return calculated growth.
     */
    def getCalculatedGrowth(prevMonthLastClosePrice: String, currMonthLastClosePrice: String): Float =
      val prevClosePrice: Float = prevMonthLastClosePrice.replace(",", ".").toFloat
      val currClosePrice: Float = currMonthLastClosePrice.replace(",", ".").toFloat
      (currClosePrice - prevClosePrice) / prevClosePrice * 100

    /**
     * Tail recursive function to iterate over the file and calculate growth.
     * Base case: growth calculation of the last month and return all calculated
     * growths and last close price of last month.
     * Recursive case: values update and growth calculation.
     *
     * @param fileContentList list with content of file.
     * @param previousMonth value of previos month.
     * @param prevMonthLastClosePrice last close price of previous month.
     * @param currMonthLastClosePrice last close price of curren month.
     * @param growthPerMonthMap map where keys are months and values are growth of
     *                          corresponding month.
     * @return map with all calculated growth and last close price of last month.
     */
    def iter(fileContentList: List[String], previousMonth: String, prevMonthLastClosePrice: String,
             currMonthLastClosePrice: String, growthPerMonthMap: Map[Int, Float]): (Map[Int, Float], Float) =

      if fileContentList.isEmpty then
        val growth: Float = getCalculatedGrowth(prevMonthLastClosePrice, currMonthLastClosePrice)
        (growthPerMonthMap + (previousMonth.toInt -> growth),
          currMonthLastClosePrice.replace(",", ".").toFloat)

      else
        val Array(dateTime, _, _, _, closePrice, _) = fileContentList.head.split(";")
        val currentMonth: String = dateTime.slice(5, 7)

        if currentMonth != previousMonth then
          val growth: Float = getCalculatedGrowth(prevMonthLastClosePrice, currMonthLastClosePrice)
          iter(fileContentList.tail, currentMonth, currMonthLastClosePrice, closePrice,
            growthPerMonthMap + (previousMonth.toInt -> growth))

        else
          iter(fileContentList.tail, previousMonth, prevMonthLastClosePrice, closePrice, growthPerMonthMap)

    val Array(dateTime, _, _, _, closePrice, _) = fileContentList.head.split(";")
    val prevMonthLastClosePrice: String =
      if initialClosePrice != -1.0 then initialClosePrice.toString
      else closePrice

    iter(fileContentList.tail, dateTime.slice(5, 7), prevMonthLastClosePrice, closePrice, Map[Int, Float]())


  /**
   * Functoion to process files according to provided processing method.
   *
   * @param filesPaths paths to files with data.
   * @param processingMethod method using which data will be processed.
   * @return list of processed files where elements are tuples of stock
   *         names, stock types, years and summary of month growth.
   */
  def getProcessedFiles(filesPaths: List[String],
                        processingMethod: String): List[(String, String, String, Map[Int, Float])] =
    var initialClosePrice: Float = -1.0f
    for pathToFile <- filesPaths yield

      val (stockName, stockType, year) = getParsedFileName(pathToFile)
      val fileContentList = getFileContentList(pathToFile)
      val growthMap =
        if processingMethod == "open-close" then
          getOpenCloseGrowth(fileContentList)

        else
          val growthMapAndLastClosePrice = getCloseCloseGrowth(fileContentList, initialClosePrice)
          initialClosePrice = growthMapAndLastClosePrice._2
          growthMapAndLastClosePrice._1

      (stockName, stockType, year, growthMap)


  /**
   * Function to insert processed data into database according to processed method.
   *
   * @param processedFiles processed data.
   * @param connectionParaments paraments for connection to database.
   * @param insertQuery insert query.
   */
  def insertProcessedFilesIntoDB(processedFiles: List[(String, String, String, Map[Int, Float])],
                                 connectionParaments: (String, String, String), insertQuery: String): Unit =
    val (user, password, url) = connectionParaments
    var connection: Connection = null

    try
      connection = DriverManager.getConnection(s"jdbc:$url", user, password)

      val statement: PreparedStatement = connection.prepareStatement(insertQuery)
      for (processedFile <- processedFiles)
        val (fileName, priceType, year, growthMap) = processedFile

        statement.setString(1, fileName)
        statement.setInt(2, year.toInt)
        statement.setBoolean(3, priceType == "Bid")

        for i <- 1 to 12 do growthMap.get(i) match
          case x: Some[Float] => statement.setFloat(i+3, growthMap(i))
          case _ => statement.setObject(i+3, null)

        val growthsValues: Array[Float] = growthMap.values.toArray
        statement.setFloat(16, growthsValues.sum/growthsValues.length)

        statement.addBatch()

      statement.executeBatch()

    catch
      case e: Exception => e.printStackTrace

    finally
      connection.close()


}
