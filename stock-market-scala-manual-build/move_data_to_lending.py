import argparse
from os import listdir
from os.path import isfile, join, exists
from subprocess import run


def get_arguments():
    parser = argparse.ArgumentParser(description="Util to load data from \
                                                  source to lending.")
    parser.add_argument('-src', '--source', type=str, required=True,
                        metavar="<source>", help='Path to data source.')
    parser.add_argument('-dst', '--destination', type=str, required=True,
                        metavar="<destination>", help='Path to data source.')
    return parser.parse_args()


def check_source_destination_existence(source, destination):
    if not exists(source):
        print(f"Directory {source} does not exists.")
        exit()
    elif not exists(destination):
        print(f"Directory {source} does not exists.")
        exit()


def check_destination_is_not_empty(destination):
    return listdir(destination)


def reload_destination_folder(destination):
    run(["rm", "-r", destination])
    run(["mkdir", destination])


def get_list_of_files(path_to_files):
    return [file for file in listdir(path_to_files)
            if isfile(join(path_to_files, file))]


def get_file_target_dir_tuple(file_name):
    stock_name, _, type, _, _ = file_name.split("_")
    return file_name, f"{stock_name}_{type}"


def get_shuffled_files(files_with_target_dir):
    shuffled_files = {}
    for file, target_dir in files_with_target_dir:
        if target_dir not in shuffled_files:
            shuffled_files[target_dir] = [file]
        else:
            shuffled_files[target_dir].append(file)
    return shuffled_files


def copy_from_src_to_dst(shuffled_files, source, destination):
    for target_dir, files in shuffled_files.items():
        run(["mkdir", f"{destination}/{target_dir}"])
        for file in files:
            run(["cp", f"{source}/{file}",
                 f"{destination}/{target_dir}/{file}"])


def main():
    arguments = get_arguments()
    source, destination = arguments.source, arguments.destination
    check_source_destination_existence(source, destination)
    destination_is_not_empty = check_destination_is_not_empty(destination)
    if destination_is_not_empty:
        reload_destination_folder(destination)
    files = get_list_of_files(source)
    files_with_target_dir = list(map(get_file_target_dir_tuple, files))
    shuffled_files = get_shuffled_files(files_with_target_dir)
    copy_from_src_to_dst(shuffled_files, source,
                         destination)


if __name__ == "__main__":
    main()
