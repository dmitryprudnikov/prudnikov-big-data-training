from argparse import ArgumentParser
from os import environ, listdir
from subprocess import run, Popen


def get_arguments():
    parser = ArgumentParser(description="Utility to do initial load.")
    parser.add_argument('-src', '--source', type=str, required=True,
                        metavar="<source>", help='Path to data source.')
    parser.add_argument('-lndg', '--lending', type=str, required=True,
                        metavar="<lending>", help='Path to future lending.')
    parser.add_argument('-cp', '--config-path', type=str, required=True,
                        metavar="<config-path>", help='Path to future lending')
    return parser.parse_args()


def run_move_data_to_lending(source, destination):
    run(["python3", "move_data_to_lending.py",
         "-src", source, "-dst", destination])


def run_database_setup(host, user, password, database):
    with open("dbSetup/createDataBase.sql") as query:
        database_setup = Popen(["mysql", "-h", host, "-u", user,
                                f"-p{password}"], stdin=query)
        database_setup.wait()

    ddl_queries = [f"dbSetup/ddl/tables/{query}"
                   for query in listdir("dbSetup/ddl/tables")]

    for ddl_query in ddl_queries:
        with open(ddl_query) as query:
            Popen(["mysql", "-h", host, "-u", user, f"-p{password}",
                   "-D", database], stdin=query)


def insert_into_avg_tables(host, user, password, database):
    dml_queries = [f"dbSetup/dml/{query}" for query in listdir("dbSetup/dml")]

    for dml_query in dml_queries:
        with open(dml_query) as query:
            Popen(["mysql", "-h", host, "-u", user, f"-p{password}",
                   "-D", database], stdin=query)


def compile():
    run(["rm", "-r", "target"])
    run(["mkdir", "target"])
    run(["scalac", "-d", "target", "-cp",
         "./lib/config-1.4.1.jar:./lib/mysql-connector-java-8.0.27.jar",
         "src/com/company/Calculator.scala"])


def run_data_processing(path_to_data, processing_method, path_to_config):
    run(["scala", "-cp", "./target", "-cp", "./lib/config-1.4.1.jar", "-cp",
         "./lib/mysql-connector-java-8.0.27.jar", "com.company.Calculator",
         "-dp", path_to_data, "-pm", processing_method,
         "--config-path", path_to_config])


def process_all(path_to_lending, processing_methods, path_to_config):
    data_dirs_paths = [f"{path_to_lending}/{dir}"
                       for dir in listdir(path_to_lending)]

    for path_to_data_dir in data_dirs_paths:
        for processing_method in processing_methods:
            run_data_processing(path_to_data_dir, processing_method,
                                path_to_config)


def main():
    args = get_arguments()
    path_to_source = args.source
    path_to_lending = args.lending
    path_to_config = args.config_path

    MYSQL_HOST = environ["MYSQL_HOST"]
    MYSQL_USER = environ["MYSQL_USER"]
    MYSQL_PASSWORD = environ["MYSQL_PASSWORD"]
    STOCK_MARKET_DATABASE = environ["STOCK_MARKET_DATABASE"]

    processing_methods = ["open-close", "close-close"]

    run_database_setup(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD,
                       STOCK_MARKET_DATABASE)

    run_move_data_to_lending(path_to_source, path_to_lending)

    compile()

    process_all(path_to_lending, processing_methods, path_to_config)

    insert_into_avg_tables(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD,
                           STOCK_MARKET_DATABASE)


if __name__ == "__main__":
    main()
