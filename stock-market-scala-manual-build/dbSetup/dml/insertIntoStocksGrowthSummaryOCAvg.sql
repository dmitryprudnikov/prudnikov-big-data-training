TRUNCATE stocksGrowthSummaryOCAvg;

INSERT INTO stocksGrowthSummaryOCAvg
SELECT name, 
	   bid,
	   AVG(january),
	   AVG(february),
	   AVG(march),
	   AVG(april),
	   AVG(may),
	   AVG(june),
	   AVG(july),
	   AVG(august),
	   AVG(september),
	   AVG(october),
	   AVG(november),
	   AVG(december),
	   AVG(average)
FROM stocksGrowthSummaryOC 
GROUP BY name, 
		 bid