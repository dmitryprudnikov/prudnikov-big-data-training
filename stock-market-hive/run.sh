#bin/bash

hdfs dfs -ls -C /user/stock_market_data/stock_market_data/fund > files.txt

while read line; do
  echo "$line"
done <files.txt


# hive -hivevar name="USA500IDXUSD" -hivevar bid=1 -hivevar year=2020 \
# -hivevar source="/user/stock_market_data/stock_market_data/fund/USA500IDXUSD_10 Secs_Bid_2020.01.01_2020.07.17.csv" \
# -f "database/dml/stock_prices_staging_load.hql"

# hive -hivevar processing_method="close-close" -f "database/dml/insert_month_stock_growth.hql"
