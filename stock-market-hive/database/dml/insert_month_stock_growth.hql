use stock_market;

set processing_method;
set hivevar:processing_method;

with
  counted_growth_data as (

    select
          name,
          bid,
          year,
          month,
          case
            when '${hivevar:processing_method}' = "open-close" then
              (close_price - open_price)
              / open_price

            when '${hivevar:processing_method}' = "close-close" then
              (close_price - lag(close_price) over name_bid_window)
              / lag(close_price) over name_bid_window

            else
              null

          end * 100 as growth

    from month_first_last_stock_prices

    window name_bid_window as (
      partition by
        name,
        bid
      order by
        year,
        month
    )

  )

insert into month_stock_growth
select
  name,
  year,
  bid,
  '${hivevar:processing_method}',
  sum(case when month = 1 then growth else null end),
  sum(case when month = 2 then growth else null end),
  sum(case when month = 3 then growth else null end),
  sum(case when month = 4 then growth else null end),
  sum(case when month = 5 then growth else null end),
  sum(case when month = 6 then growth else null end),
  sum(case when month = 7 then growth else null end),
  sum(case when month = 8 then growth else null end),
  sum(case when month = 9 then growth else null end),
  sum(case when month = 10 then growth else null end),
  sum(case when month = 11 then growth else null end),
  sum(case when month = 12 then growth else null end)

from counted_growth_data

group by
  name,
  bid,
  year;
