use stock_market;

insert into month_stock_growth_avg
select
  name,
  bid,
  processing_method,
  avg(january),
  avg(february),
  avg(march),
  avg(april),
  avg(may),
  avg(june),
  avg(july),
  avg(august),
  avg(september),
  avg(october),
  avg(november),
  avg(december)
from month_stock_growth
group by
  name,
  bid,
  processing_method;
