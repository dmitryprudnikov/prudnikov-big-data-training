use stock_market;

insert into month_first_last_stock_prices

select distinct
      name,
      bid,
      year,
      month(time_utc) as month,
      cast(regexp_replace(
        first_value(open_price)
          over name_bid_year_month_window,
        ',', '.') as float),
      cast(regexp_replace(
        last_value(close_price)
          over name_bid_year_month_window,
        ',', '.') as float)
from stock_prices_staging
where month(time_utc) is not null
window name_bid_year_month_window as (
  partition by
    name,
    bid,
    year,
    month(time_utc)
  order by time_utc
  range between unbounded preceding and unbounded following
);
