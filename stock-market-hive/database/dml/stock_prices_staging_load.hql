use stock_market;

set source;
set name;
set bid;
set year;
set hivevar:source;
set hivevar:name;
set hivevar:bid;
set hivevar:year;

load data inpath '${hivevar:source}'
into table stock_prices_staging
partition (
  name='${hivevar:name}',
  bid='${hivevar:bid}',
  year='${hivevar:year}'
);
