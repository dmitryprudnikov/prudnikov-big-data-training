use stock_market;

drop table if exists month_stock_growth;

create table month_stock_growth (
  name string,
  year smallint,
  bid tinyint,
  processing_method string,
  january float,
  february float,
  march float,
  april float,
  may float,
  june float,
  july float,
  august float,
  september float,
  october float,
  november float,
  december float
)

row format delimited
fields terminated by ';'
lines terminated by '\n'

stored as textfile;
