use stock_market;

drop table if exists stock_prices_staging;

create table stock_prices_staging (
  time_utc timestamp,
  open_price string,
  high_price string,
  low_price string,
  close_price string,
  volume string
)

partitioned by (
  name string,
  bid tinyint,
  year smallint
)

row format delimited
fields terminated by ';'
lines terminated by '\n'

stored as textfile;
