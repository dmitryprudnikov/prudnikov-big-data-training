use stock_market;

drop table if exists month_first_last_stock_prices;

create table month_first_last_stock_prices (
  name string,
  bid tinyint,
  year smallint,
  month tinyint,
  open_price float,
  close_price float
)

row format delimited
fields terminated by ';'
lines terminated by '\n'

stored as textfile;
