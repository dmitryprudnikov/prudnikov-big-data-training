package com.company;

import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Utility to calculate growth per month of stock for every year.
 *
 * @author Dmitry Prudnikov
 */
public class Main {


    /**
     * Program entry point.
     *
     * @param args provided command line arguments.
     */
    public static void main(String[] args) {

        String MYSQL_USER = System.getenv("MYSQL_USER");
        String MYSQL_PASSWORD = System.getenv("MYSQL_PASSWORD");
        String MYSQL_STOCK_MARKET_DATABASE = System.getenv("MYSQL_STOCK_MARKET_DATABASE");

        String[] options = getParsedOptions(args);
        String pathToDataDir = options[0];
        String processingMethod = options[1];

        Path dataDir = Paths.get(pathToDataDir);

        Float initialClosePrice = null;
        AtomicBoolean initial = new AtomicBoolean(true);
        AtomicReference<Float> lastClosePrice = new AtomicReference<>();

        try {
            Files.walk(dataDir).forEach(file ->{
                if(file.toFile().isFile()){
                    String fileName = file.getFileName().toString();
                    String stockName = getStockName(fileName);
                    String year = getYear(fileName);
                    Map<Integer, Float> growthPerMonth = new HashMap<>();

                    if (processingMethod.equals("open-close")){
                        growthPerMonth = getOpenCloseGrowthPerMonth(file.toString());

                    } else if (processingMethod.equals("close-close")){
                        if (initial.get()){
                            Object[] growthAndLastClosePrice = getCloseCloseGrowthPerMonth(file.toString(), initialClosePrice);
                            growthPerMonth = (Map<Integer, Float>) growthAndLastClosePrice[0];
                            lastClosePrice.set((Float) growthAndLastClosePrice[1]);
                            initial.set(false);

                        } else {
                            Object[] growthAndLastClosePrice = getCloseCloseGrowthPerMonth(file.toString(), lastClosePrice.get());
                            growthPerMonth = (Map<Integer, Float>) growthAndLastClosePrice[0];
                            lastClosePrice.set((Float) growthAndLastClosePrice[1]);

                        }

                    }

                    Object[] insertionParams = {stockName, year, growthPerMonth};
                    insertIntoDB(MYSQL_STOCK_MARKET_DATABASE, MYSQL_USER, MYSQL_PASSWORD, insertionParams);

                }

            });

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }


    /**
     * Method to get parsed command line options.
     *
     * @param args Flags with their values provided in the command line.
     * @return Array of string with parsed options.
     */
    public static String[] getParsedOptions(String[] args){
        Options options = new Options();

        Option dataDirOption = new Option("dd", "data-dir", true,
                "Directory with data which will be processed.");
        dataDirOption.setRequired(true);
        options.addOption(dataDirOption);

        Option processingMethodOption = new Option("pm", "processing-method", true,
                "Method by which data will be processed.\n " +
                        "Only two flags are available: <open-close> or <close-close>");
        processingMethodOption.setRequired(true);
        options.addOption(processingMethodOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine commandLine = null;

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("get-summary", options);
            System.exit(1);
        }

        String dataDir = commandLine.getOptionValue("data-dir");
        String processingMethod = commandLine.getOptionValue("processing-method");

        if ((!processingMethod.equals("open-close")) & (!processingMethod.equals("close-close"))){
            System.out.println("Unknown processing method: " + processingMethod);
            formatter.printHelp("get-summary", options);
            System.exit(1);
        }

        return new String[]{dataDir, processingMethod};

    }


    /**
     * Method to get name of stock from file name.
     *
     * @param fileName name of file which will be parsed.
     * @return parsed stock name.
     */
    public static String getStockName(String fileName){
        return fileName.split("_")[0];
    }


    /**
     * Method to get year data from file name.
     *
     * @param fileName name of file which will be parsed.
     * @return parsed year.
     */
    public static String getYear(String fileName){
        Pattern datePattern = Pattern.compile("\\d{4}\\.\\d{2}\\.\\d{2}");
        Matcher dateMatcher = datePattern.matcher(fileName);
        String year = null;
        if (dateMatcher.find()) {
            int start = dateMatcher.start();
            year = fileName.substring(start, start + 4);
        }

        return year;
    }


    /**
     * Method to calculate growth per every month of stock using open-close way.
     * To get growth are implemented next calculations:
     * difference between last close price in month and first open price in month
     * is divided by first open price in month.
     *
     * @param file file with data on which calculations will be carried out.
     * @return calculated growth per every month.
     */
    public static Map<Integer, Float> getOpenCloseGrowthPerMonth(String file){
        Map<Integer, Float> growthPerMonthMap = new HashMap<>();

        BufferedReader reader = null;

        try{
            reader = new BufferedReader(new FileReader(file));
            String currentLine;
            String previousMonth = null;
            Float currentOpenPrice = null;
            Float currentClosePrice = null;

            while ((currentLine = reader.readLine()) != null){
                String[] splitLine = currentLine.split(";");

                if (splitLine[0].equals("Time (UTC)")){
                    continue;
                }

                String currentMonth = splitLine[0].substring(5, 7);

                if (previousMonth == null){
                    currentOpenPrice = Float.parseFloat(splitLine[1].replace(",", "."));

                } else if (!previousMonth.equals(currentMonth)){
                    Float growth = (currentClosePrice - currentOpenPrice) / currentOpenPrice * 100;
                    growthPerMonthMap.put(Integer.parseInt(previousMonth), growth);
                    currentOpenPrice = Float.parseFloat(splitLine[1].replace(",", "."));
                }

                previousMonth = currentMonth;
                currentClosePrice = Float.parseFloat(splitLine[4].replace(",", "."));

            }

            Float growth = (currentClosePrice - currentOpenPrice) / currentOpenPrice * 100;
            growthPerMonthMap.put(Integer.parseInt(previousMonth), growth);

        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);

        } finally {
            try {
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

        }

        return growthPerMonthMap;
    }


    /**
     * Method to calculate growth per every month of stock using close-close way.
     * To get growth are implemented next calculations:
     * difference between last close price in current month and last close price in month before
     * is divided by last close price in month before.
     *
     * @param file file with data on which calculations will be carried out.
     * @param initialClosePrice last close price for calculation for first month.
     * @return array of objects where first element is map with calculated growth for every month
     * and second element is last close price for calculation in following files.
     */
    public static Object[]  getCloseCloseGrowthPerMonth(String file, Float initialClosePrice){
        Object[] growthAndLastPrice = new Object[2];
        Map<Integer, Float> growthPerMonthMap = new HashMap<>();

        BufferedReader reader = null;

        try{
            reader = new BufferedReader(new FileReader(file));
            String currentLine;
            String previousMonth = null;
            Float currentClosePrice = null;
            Float previousClosePrice = initialClosePrice;

            while ((currentLine = reader.readLine()) != null){
                String[] splitLine = currentLine.split(";");

                if (splitLine[0].equals("Time (UTC)")){
                    continue;
                }

                String currentMonth = splitLine[0].substring(5, 7);

                if (previousMonth == null){
                    previousMonth = currentMonth;
                    continue;

                } else if (!previousMonth.equals(currentMonth) & previousClosePrice == null){
                    growthPerMonthMap.put(Integer.parseInt(previousMonth), null);
                    previousClosePrice = currentClosePrice;

                } else if (!previousMonth.equals(currentMonth)){
                    Float growth = (currentClosePrice - previousClosePrice) / previousClosePrice * 100;
                    growthPerMonthMap.put(Integer.parseInt(previousMonth), growth);
                    previousClosePrice = currentClosePrice;
                }

                previousMonth = currentMonth;
                currentClosePrice =  Float.parseFloat(splitLine[4].replace(",", "."));

            }

            Float growth = (currentClosePrice - previousClosePrice) / previousClosePrice * 100;
            growthPerMonthMap.put(Integer.parseInt(previousMonth), growth);

            growthAndLastPrice[0] = growthPerMonthMap;
            growthAndLastPrice[1] = currentClosePrice;

        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);

        } finally {
            try {
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

        }

        return growthAndLastPrice;

    }


    /**
     * Method to insert obtained summary (stock name, year, calculated growth) into database.
     *
     * @param url location of database.
     * @param user database user with permission to insert data.
     * @param password user password.
     * @param summary array of objects with obtained summary (stock name, year, calculated growth).
     */
    public static void insertIntoDB(String url, String user, String password, Object[] summary){
        String INSERTION = "INSERT INTO stockGrowthSummary VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        String stockName = (String) summary[0];
        String year = (String) summary[1];
        Map<Integer, Float> growthPerMonth = (Map<Integer, Float>) summary[2];

        try {
            Connection connection = DriverManager.getConnection("jdbc:" + url, user, password);
            PreparedStatement insert = connection.prepareStatement(INSERTION);

            insert.setString(1, stockName);
            insert.setString(2,year);
            for(int i=1; i<=12; i++){
                insert.setObject(i+2, growthPerMonth.get(i));
            }

            insert.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }


}
