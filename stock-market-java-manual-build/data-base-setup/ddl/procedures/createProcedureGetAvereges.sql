DROP PROCEDURE IF EXISTS getAvereges;

delimiter $$
CREATE PROCEDURE getAvereges()
BEGIN

	SELECT *
    FROM stockGrowthSummary

    UNION ALL

    SELECT "Average:",
	   "",
	   AVG(january),
	   AVG(february),
       AVG(march),
       AVG(april),
	   AVG(may),
       AVG(june),
       AVG(july),
	   AVG(august),
       AVG(september),
	   AVG(october),
       AVG(november),
       AVG(december)
	FROM stockGrowthSummary;

END$$
delimiter ;
