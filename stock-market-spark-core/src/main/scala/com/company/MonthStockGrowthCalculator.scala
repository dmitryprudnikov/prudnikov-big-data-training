package com.company

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import java.io.File
import java.sql.{Connection, DriverManager, PreparedStatement}
import scala.annotation.tailrec
import scala.sys.{env, exit}

object MonthStockGrowthCalculator {

  def main(args: Array[String]): Unit = {

    val (source, config, processingMethod) = getParsedArguments(args)

    val (databaseConfig, sparkConfig) = getConfig(config)

    val dbConnectionParameters = getDBConnectionParameters(databaseConfig)
    val insertTemplate = getInsertTemplate(databaseConfig)

    val sparkContext = getSparkContext(sparkConfig)
    sparkContext.setLogLevel(sparkConfig.getString("log-level"))

    val nameAndType = getNameAndType(source)
    val fileContent = getFileContent(source, sparkContext)

    val monthFirstLastStockPricesMetrics = getMonthFirstLastStockPricesMetrics(fileContent)
    val stockPricesMonthGrowthPerYears =
      getStockPricesMonthGrowthPerYears(monthFirstLastStockPricesMetrics, processingMethod)

    insertMonthStockGrowthIntoDB(dbConnectionParameters, insertTemplate,
      processingMethod, nameAndType, stockPricesMonthGrowthPerYears)

  }


  def getParsedArguments(args: Array[String]): (String, String, String) = {
    @tailrec
    def nextArgument(remainingArgs: List[String],
                     parsedArgs: Map[String, String]): (String, String, String) = {
      remainingArgs match {
        case Nil =>
          (parsedArgs("source"), parsedArgs("config"), parsedArgs("processingMethod"))

        case ("-src" | "--source") :: value :: tail =>
          nextArgument(tail, parsedArgs + ("source" -> value))

        case ("-c" | "--config") :: value :: tail =>
          nextArgument(tail, parsedArgs + ("config" -> value))

        case ("-pm" | "--processing-method") :: value :: tail =>
          nextArgument(tail, parsedArgs + ("processingMethod" -> value))

        case argument :: _ => println(s"Unknown argument: $argument")
          exit(1)

      }

    }

    val usage: String =
      "Usage: MonthStockGrowthCalculator -src <source> -c <config> -pm <processingMethod>"

    if (args.length != 6) {
      println(usage)
      exit(1)
    }

    nextArgument(args.toList, Map())

  }


  def getConfig(configPath: String): (Config, Config) = {
    val config = ConfigFactory.parseFile(new File(configPath)).getConfig("app")

    val databaseConfig = config.getConfig("database")
    val sparkConfig = config.getConfig("spark")

    (databaseConfig, sparkConfig)

  }


  def getDBConnectionParameters(config: Config): (String, String, String) = {
    val user: String = env(config.getString("username"))
    val password: String = env(config.getString("password"))
    val url: String = env(config.getString("url"))

    (user, password, url)
  }


  def getInsertTemplate(config: Config): String = {
    config.getConfig("template-statements").getString("insert-into-month-stock-growth")
  }


  def getSparkContext(config: Config): SparkContext =
    SparkContext.getOrCreate(new SparkConf()
      .setAppName(config.getString("app-name"))
      .setMaster(config.getString("master")))


  def getNameAndType(source: String): (String, String) = {
    val dirName = source.substring(source.lastIndexOf("/") + 1)
    val Array(stockName, priceType) = dirName.split("_")
    (stockName, priceType)

  }


  def getFileContent(source: String, sc: SparkContext): RDD[String] = {
    val file = sc.textFile(source)
    val header = file.first

    if (header.startsWith("Time (UTC)")) {
      file.filter(line => line != header)
    } else {
      file
    }
  }


  def getMonthFirstLastStockPricesMetrics(fileContent: RDD[String]): RDD[(String, (Float, Float))] = {

    def getYearAndMonth(line: String): String = {
      line.slice(0, 7)
    }

    def getOpenClosePricesMetrics(metrics: String): (String, String) = {
      val Array(_, openPrice, _, _, closePrice, _) = metrics.split(";")
      (openPrice, closePrice)
    }

    def getReducedPricesMetrics(left: (String, String),
                                right: (String, String)): (String, String) = {
      val (leftOpenPrice, _) = left
      val (_, rightClosePrice) = right
      (leftOpenPrice, rightClosePrice)
    }

    def getFloatPrices(prices: (String, String)): (Float, Float) = {
      val (openPrice, closePrice) = prices
      val openPriceFloat = openPrice.replace(",", ".").toFloat
      val closePriceFloat = closePrice.replace(",", ".").toFloat
      (openPriceFloat, closePriceFloat)
    }

    fileContent.keyBy(getYearAndMonth)
      .mapValues(getOpenClosePricesMetrics)
      .reduceByKey(getReducedPricesMetrics)
      .mapValues(getFloatPrices)


  }


  def getStockPricesMonthGrowthPerYears(monthFirstLastStockPricesMetrics: RDD[(String, (Float, Float))],
                                        processingMethod: String):  RDD[(String, Iterable[(Int, Float)])] = {

    def getCalculatedGrowth(basePrice: Float, currentPrice: Float): Float = {
      (currentPrice - basePrice) / basePrice * 100
    }

    def getOpenCloseGrowth(metrics: RDD[(String, (Float, Float))]): RDD[(String, Float)] = {
      metrics.mapValues({ case (open, close) => getCalculatedGrowth(open, close) })
    }

    def getCloseCloseGrowth(metrics: RDD[(String, (Float, Float))]): RDD[(String, Float)] = {
      val indexedMetrics =
        metrics.sortByKey()
          .zipWithIndex()
          .map({ case ((monthAndYear, (_, close)), idx) => (idx, (monthAndYear, close)) })

      val indexedByIncreasedByOneValMetrics = indexedMetrics.map({
        case (idx, (monthAndYear, close)) => (idx + 1, (monthAndYear, close))
      })

      val metricsWithPreviousClosePrice = indexedMetrics.leftOuterJoin(indexedByIncreasedByOneValMetrics)

      metricsWithPreviousClosePrice.map({
        case (_, ((monthAndYear, currentPrice), baseMetric)) => baseMetric match {
          case Some((_, basePrice)) =>
            (monthAndYear, getCalculatedGrowth(basePrice, currentPrice))

          case None => (monthAndYear, 0.0f)
        }
      })

    }

    def getMetricsWithYearKey(metrics: (String, Float)): (String, (Int, Float)) = {
      val (yearAndMonth, growth) = metrics
      val Array(year, month) = yearAndMonth.split("-")
      (year, (month.toInt, growth))
    }

    val monthGrowthMetrics = processingMethod match {
      case "open-close" => getOpenCloseGrowth(monthFirstLastStockPricesMetrics)
      case "close-close" => getCloseCloseGrowth(monthFirstLastStockPricesMetrics)
      case _ => println("Unknown processing method."); sys.exit(1)
    }

    monthGrowthMetrics.map(getMetricsWithYearKey).groupByKey

  }


  def getBatchInsertStatement(connection: Connection, insertTemplate: String,
                              processingMethod: String,
                              stockNameAndType: (String, String),
                              growthMetrics: RDD[(String, Iterable[(Int, Float)])]): PreparedStatement = {

    val batchInsertStatement = connection.prepareStatement(insertTemplate)

    val (stockName, stockType) = stockNameAndType
    batchInsertStatement.setString(1, stockName)
    batchInsertStatement.setBoolean(2, stockType == "bid")
    batchInsertStatement.setString(3, processingMethod)

    for (yearMetrics <- growthMetrics.collect){
      val (year, metrics) = yearMetrics
      batchInsertStatement.setString(4, year)

      val metricsMap = metrics.toMap
      for (i <- 1 to 12){
        metricsMap.get(i) match {
          case Some(growth) => batchInsertStatement.setFloat(i + 4, growth)
          case None => batchInsertStatement.setNull(i + 4, 0)
        }

      }

      batchInsertStatement.addBatch()

    }

    batchInsertStatement

  }


  def insertMonthStockGrowthIntoDB(connectionParameters: (String, String, String),
                                   insertTemplate: String,
                                   processingMethod: String,
                                   stockNameAndType: (String, String),
                                   growthMetrics: RDD[(String, Iterable[(Int, Float)])]): Unit = {
    val (user, password, url) = connectionParameters
    var connection: Connection = null
    try {
      connection = DriverManager.getConnection(s"jdbc:$url", user, password)

      val batchInsertStatement =
        getBatchInsertStatement(connection, insertTemplate, processingMethod, stockNameAndType, growthMetrics)
      batchInsertStatement.executeBatch()

    } catch {
      case e: Exception => e.printStackTrace()
        exit(1)

    } finally {
      connection.close()
    }

  }


}
