drop table if exists month_stock_growth_avg;
create table month_stock_growth_avg (
	name varchar(20),
	bid tinyint(1),
	processing_method varchar(20),
	january decimal(7,4),
	february decimal(7,4),
	march decimal(7,4),
	april decimal(7,4),
	may decimal(7,4),
	june decimal(7,4),
	july decimal(7,4),
	august decimal(7,4),
	september decimal(7,4),
	october decimal(7,4),
	november decimal(7,4),
	december decimal(7,4),
	average decimal(7,4),
	primary key (name, bid, processing_method)
);
